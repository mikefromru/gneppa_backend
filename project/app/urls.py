from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import views

urlpatterns = [
	path('', views.index),
	path('root/', views.LevelListView.as_view(), name='root'),
	path('root/detail/<int:id>/', views.detail, name='detail'),
	path('root/getfile/<int:id>/', views.get_file, name='get_file'),
	path('root/search/', views.search, name='search'),

]


