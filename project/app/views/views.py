from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from ..serializers import LevelSerializer

from ..models import Level
from ..models import Question

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

@method_decorator(staff_member_required, name='dispatch')
class LevelListView(ListView):

	model = Level
	paginate_by = 50

@staff_member_required
def detail(request, id):
	q = Question.objects.filter(level=id).values()
	bar = [x for x in q if x.get('approved')]

	return render(request, 'app/detail.html', {'q': bar})

@staff_member_required
def get_file(request, id):
	if request.method == 'POST':
		bar = Question.objects.get(id=id)
		bar.robot_voice = request.FILES['filename']
		bar.save()
		return redirect(request.META['HTTP_REFERER'])
	else:
		return HttpResponse('something went wrong')

@staff_member_required
def search(request):
	try:
		q = request.GET['q']
		name = Level.objects.filter(name__contains=q)
		return render(request, 'app/search_result.html', {'name': name})
	except KeyError:
		return render('app/search_result.html')

@staff_member_required
def index(request):
	queryset = Level.objects.all()[:5]
	return render(request, 'app/index.html', {'serializer': LevelSerializer(queryset, many=True).data})
