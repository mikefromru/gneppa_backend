from rest_framework import serializers
from . models import Level, Question

class LevelSerializer(serializers.ModelSerializer):

    class Meta:

        model = Level
        fields = '__all__'

        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
        


class QuestionSerializer(serializers.ModelSerializer):

    class Meta:

        model = Question
        fields = '__all__'
