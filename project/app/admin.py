from django.contrib import admin
from . models import Level, Question, UserLevel

@admin.register(UserLevel)
class UserLevelAdmin(admin.ModelAdmin):
    pass


@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):

    search_fields = ('name',)


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):

    search_fields = ('id', 'name')
