from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User


from django.core.exceptions import ValidationError

class Level(models.Model):

    def validate_image(filename):
        filesize = filename.file.size
        megabyte_limit = 0.005 # 5 kilobyte
        if filesize > megabyte_limit*1024*1024:
            raise ValidationError("Max file size is {5}KB")


    name = models.CharField(max_length=100, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=True)
    slug = models.SlugField(max_length=250, unique=True, null=True, blank=True)
    image = models.ImageField(upload_to='i/image_levels/', validators=[validate_image], blank=True, null=True)
    
    def __str__(self):
        return self.name 

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Level, self).save(*args, **kwargs)

class UserLevel(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    favorite = models.BooleanField(default=False)
    progress = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.level, self.user.username

def content_file_name(instance, filename):
    return "sounds/{instance}/{file}".format(instance=instance.level, file=filename)

class Question(models.Model):

    level = models.ForeignKey(Level, on_delete=models.CASCADE)
    name = models.CharField(max_length=700)
    approved = models.BooleanField(default=True)
    robot_voice = models.FileField(upload_to=content_file_name, blank=True, null=True)

    def __str__(self):
        return self.name
    
